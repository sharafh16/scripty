<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Story;
use App\User;
class Project extends Model
{
    protected $guarded = [''];

    public function user () {
        return $this->belongsTo(User::class);
    }

    public function stories() {
        return $this->hasMany(Story::class);
    }

}
