<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Project;
use App\Fact;
use App\Scene;

class Story extends Model
{
    protected $guarded = [''];
    public function project () {
        return $this->belongsTo(Project::class);
    }

    public function facts () {
        return $this->hasMany(Fact::class);
    }

    public function scenes () {
        return $this->hasMany(Scene::class);
    }
}
