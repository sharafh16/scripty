<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Shoot;
use App\Interview;
use App\Story;

class Scene extends Model
{
    protected $guarded = [''];
    
    public function story () {
        return $this->belongsTo(Story::class);
    }

    public function interview () {
        return $this->belongsTo(Interview::class);
    }

    public function shoot () {
        return $this->belongsTo(Shoot::class);
    }
}
