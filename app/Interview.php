<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Interview;
use App\Scene;

class Interview extends Model
{
    protected $guarded = [''];


    public function scenes () {
        return $this->hasMany(Scene::class);
    }

    public function interviewee () {
        return $this->hasOne(Interview::class);
    }
    
}
