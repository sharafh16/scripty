<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Story;

class Fact extends Model
{
    protected $guarded = [''];

    public function story () {
        return $this->belongsTo(Story::class);
    }

}
