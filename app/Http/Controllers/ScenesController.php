<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Scenes;

class ScenesController extends Controller
{
   
    public function index()
    {
        $scenes = Scenes::all();
        return view('scenes')->with('scenes', $scenes);
    }

   

    
    public function Addtostore(Request $request)
    {
        $file = $request->file('file');
        $filename = rand().'.'.$file->getClientOriginalExtension();
        $destination_path = public_path().'/scenes';
        $file->move($destination_path,$filename);

        $scenes = new Scenes();
        $scenes->Description = $request->Description;
        $scenes->Types = $request->Types;
        $scenes->GFX = $request->GFX;
        $scenes->file = $filename;
        $scenes->notes = $request->notes;
        $scenes->save();

        return redirect()->back();

    }

   
    public function show($id)
    {
    }

    public function edit($id)
    { 
        $scenes = Scenes::findOrFail($id);
        return view('update')->with('scenes', $scenes);
    }

   
    public function update(Request $request, $id)
    {
        $file = $request->file('file');
        $filename = rand().'.'.$file->getClientOriginalExtension();
        $destination_path = public_path().'/stories';
        $file->move($destination_path, $filename);

        $new = array(
            'Description' => $request->Description,
            'Types' => $request->Types,
            'GFX' => $request->GFX,
            'file' => $filename,
            'notes' => $request->notes
        );
        scenes::whereId($id)->update($new);

        return redirect('/scenes');

    }

  
    public function destroy($id)
    {
        $scenes = scenes::findOrFail($id);
        $scenes->delete();

        return redirect()->back();
    }
}
