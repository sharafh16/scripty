<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Interview;

class InterviewController extends Controller
{
   
    public function index()
    {
        $Interview = Interview::all();       
        return view('Interview')->with('Interview', $Interview);
    }

    
    public function AddInterview(Request $request)
    {
        
        $Interview = new Interview();
        $Interview->Name = $request->Name;
        $Interview->Questions = $request->Questions;
        $Interview->ExpectedAnswer = $request->ExpectedAnswer;
        $Interview->save();

        return redirect()->back();

    }

   
    public function show($id)
    {
    }

    public function edit($id)
    { 
        $Interview = Interview::findOrFail($id);
        return view('updateInterview')->with('Interview', $Interview);
    }

   
    public function update(Request $request, $id)
    {
       
        $new = array(
            'Name' => $request->Name,
            'Questions' => $request->Questions,
            'ExpectedAnswer' => $request->ExpectedAnswer
        );
        Interview::whereId($id)->update($new);

        return redirect('/Interview');

    }

  
    public function destroy($id)
    {
        $Interview = Interview::findOrFail($id);
        $Interview->delete();

        return redirect()->back();
    }
}
