<?php

namespace App\Http\Controllers;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;
use App\Fact;
use App\Project;
class InfoGatheringController extends Controller
{
   
    public function index(Request $request)
    {
        // $request->project_id
        $InfoGathering = Fact::all();       
        return view('InfoGathering')->with('InfoGathering', $InfoGathering);
    }

    
    public function AddInfo(Request $request)
    {
        
        $InfoGathering = new Fact();
        $InfoGathering->Citation = $request->Citation;
        $InfoGathering->Implied_Information = $request->Implied_Information;
        $InfoGathering->References = $request->References;
        $InfoGathering->save();

        return redirect()->back();

    }

   
    public function show($id)
    {
    }

    public function editInfo($id)
    { 
        $InfoGathering = Fact::findOrFail($id);
        return view('updateInfo')->with('InfoGathering', $InfoGathering);
    }

   
    public function updateInfo(Request $request, $id)
    {
       
        $new = array(
            'Citation' => $request->Citation,
            'Implied_Information' => $request->Implied_Information,
            'References' => $request->References
        );
        Fact::whereId($id)->update($new);

        return redirect('/InfoGathering');

    }

  
    public function deInfo($id)
    {
        $InfoGathering = Fact::findOrFail($id);
        $InfoGathering->delete();

        return redirect()->back();
    }
}
