<?php

namespace App\Http\Controllers;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Session;
use App\Story;
use Illuminate\Http\Request;

class StoryController extends Controller
{
    public function index()
    {
        return view('stories.create-index', ['stories' => Story::paginate(5)]);
    }

    public function create($project_id, Request $request)
    {
        Session::put('project_id',$project_id);
        return view('stories.create-index');
    }

    public function store(Request $request)
    {
        Story::create([
            'description' => $request->description, 
            'project_id' =>Session::get('project_id')
        ]);
        return redirect()->route('stories.index')
                            ->with('success', 'CREATED SUCCESSFULLY');
    }

    public function show(Story $story)
    {

        return view('stories.show', ['story' => $story] );
    }

    public function edit(Story $story)
    {
        return view('stories.edit', ['story' => $story]);
    }

    public function update()
    {
        Session::push('story_id', request('story_id'));
        Story::findOrFail(request('story_id'))->update(Arr::except(request()->all(), 'story_id', '_token', '_method'));
        return redirect()->route('stories.index')
                            ->with('success', 'UPDATED SUCCESSFULLY');
    }


    public function destroy(Story $story)
    {
        Story::find($story->id)->delete();

        return redirect()->back()->with('success', 'Deleted Successfully');
    }
}
