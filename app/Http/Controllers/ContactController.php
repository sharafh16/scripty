<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contact;

class ContactController extends Controller
{
   
    public function index()
    {
        $Contact = Contact::all();       
        return view('Contact')->with('Contact', $Contact);
    }

    
    public function AddtoContact(Request $request)
    {
        
        $Contact = new Contact();
        $Contact->Name = $request->Name;
        $Contact->Title = $request->Title;
        $Contact->Email = $request->Email;
        $Contact->Phone = $request->Phone;
        $Contact->Status = $request->Status;
        $Contact->Date = $request->Date;
        $Contact->Time = $request->Time;
       
        $Contact->save();

        return redirect()->back();

    }

   
    public function show($id)
    {
    }

    public function edit($id)
    { 
        $Contact = Contact::findOrFail($id);
        return view('updateContact')->with('Contact', $Contact);
    }

   
    public function update(Request $request, $id)
    {
       
        $new = array(
            'Name' => $request->Name,
            'Title' => $request->Title,
            'Email' => $request->Email,
            'Phone' => $request->Phone,
            'Status' => $request->Status,
            'Date' => $request->Date,
            'Time' => $request->Time,
           

        );
        Contact::whereId($id)->update($new);

        return redirect('/Contact');

    }

  
    public function destroy($id)
    {
        $Contact = Contact::findOrFail($id);
        $Contact->delete();

        return redirect()->back();
    }
}
