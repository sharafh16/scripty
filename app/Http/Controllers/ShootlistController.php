<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Shootlist;

class ShootlistController extends Controller
{
   
    public function index()
    {
        $Shootlist = Shootlist::all();       
        return view('Shootlist')->with('Shootlist', $Shootlist);
    }

    
    public function AddShootlist(Request $request)
    {
        
        $Shootlist = new shootlist();
        $Shootlist->SceneNumber = $request->SceneNumber;
        $Shootlist->Date = $request->Date;
        $Shootlist->Time = $request->Time;
        $Shootlist->Location = $request->Location;
        $Shootlist->Permissions = $request->Permissions;
        $Shootlist->save();

        return redirect()->back();

    }

   
    public function show($id)
    {
    }

    public function edit($id)
    { 
        $Shootlist = Shootlist::findOrFail($id);
        return view('updateShootlist')->with('Shootlist', $Shootlist);
    }

   
    public function update(Request $request, $id)
    {
       
        $new = array(
            'SceneNumber' => $request->SceneNumber,
            'Date' => $request->Date,
            'Time' => $request->Time,
            'Locations' => $request->Locations,
            'Permissions' => $request->Permissions,

        );
        Shootlist::whereId($id)->update($new);

        return redirect('/Shootlist');

    }

  
    public function destroy($id)
    {
        $Shootlist = Shootlist::findOrFail($id);
        $Shootlist->delete();

        return redirect()->back();
    }
}
