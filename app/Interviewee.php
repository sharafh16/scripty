<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Interview;

class Interviewee extends Model
{
    protected $guarded = [''];

    public function interview () {
        return $this->belongsTo(Interview::class);
    }
}
