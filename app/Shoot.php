<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Scene;
class Shoot extends Model
{
    protected $guarded = [''];
    
    public function scenes () {
        return $this->hasMany(Scene::class);
    }
}
