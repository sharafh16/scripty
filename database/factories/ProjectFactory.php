<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Project;
use Faker\Generator as Faker;

$factory->define(Project::class, function (Faker $faker) {
    return [
        'title' => 'My Project Title',
        'description' => 'My project desc',
        'duration' => 23,
        'completed_at' => '2020-03-19 00:00:00',
        'started_at' => '2020-03-15 00:00:00',
        'user_id' => factory(App\User::class),
    ];
});