<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInterviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    { 
        Schema::create('interviews', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->string('question');
            $table->string('expected_answer');
            $table->unsignedBigInteger('interviewee_id');
            $table->foreign('interviewee_id')->references('id')->on('interviewees')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('interviews');
    }
}
