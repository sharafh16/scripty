<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateScenesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scenes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('description');
            $table->string('type');
            $table->string('GFX');
            $table->string('file');
            $table->string('notes');
            $table->unsignedBigInteger('story_id');
            $table->foreign('story_id')->references('id')->on('stories')->onDelete('cascade');
            $table->unsignedBigInteger('interview_id');
            $table->foreign('interview_id')->references('id')->on('interviews')->onDelete('cascade');
            $table->unsignedBigInteger('shoot_id');
            $table->foreign('shoot_id')->references('id')->on('shoots')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scenes');
    }
}
