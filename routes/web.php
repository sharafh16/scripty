<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('projects/index', 'ProjectController@index')->name('projects.index'); //route
Route::get('projects/create', 'ProjectController@create')->name('projects.create');  //route
Route::post('projects/store', 'ProjectController@store')->name('projects.store'); //form only
Route::get('projects/{project}/show', 'ProjectController@show')->name('projects.show'); //wildcard
Route::get('projects/{project}/edit', 'ProjectController@edit')->name('projects.edit'); //wildcard
Route::patch('projects/update', 'ProjectController@update')->name('projects.update'); //form hidden
Route::delete('projects/{project}/delete', 'ProjectController@destroy')->name('projects.delete'); //form with wildcard

Route::get('stories/index', 'StoryController@index')->name('stories.index'); //route
Route::get('stories/{project}/create', 'StoryController@create')->name('stories.create');  //route
Route::post('stories/store', 'StoryController@store')->name('stories.store'); //form only
Route::get('stories/{story}/show', 'StoryController@show')->name('stories.show'); //wildcard
Route::get('stories/{story}/edit', 'StoryController@edit')->name('stories.edit'); //wildcard
Route::patch('stories/update', 'StoryController@update')->name('stories.update'); //form hidden
Route::delete('stories/{story}/delete', 'StoryController@destroy')->name('stories.delete'); //form with wildcard



// Route::post('/InformationGathering', 'InfoGatheringController@index');
// Route::post('/AddInfo', 'InfoGatheringController@AddInfo');
// Route::delete('/deInfo/{id}', 'InfoGatheringController@deInfo');
// Route::post('/editInfo/{id}', 'InfoGatheringController@editInfo');
// Route::patch('InfoGathering/updateInfo/{id}', 'InfoGatheringController@updateInfo')->name('InfoGathering.updateInfo');
// Route::get('/InfoGathering', 'InfoGatheringController@index');







 
// Route::get('/StoryScript', 'ScenesController@index');
// Route::post('/Addtostore', 'ScenesController@Addtostore');
// Route::delete('/delete/{id}', 'ScenesController@destroy');
// Route::post('/edit/{id}', 'ScenesController@edit');
// Route::patch('story/update/{id}', 'ScenesController@update')->name('story.update');
// Route::get('/story', 'SScenesController@index');


// Route::get('/Shootlist', 'ShootlistController@index');
// Route::post('/AddShootlist', 'ShootlistController@AddShootlist');
// Route::delete('/delete/{id}', 'ShootlistController@destroy');
// Route::post('/edit/{id}', 'ShootlistController@edit');
// Route::patch('Shootlist/update/{id}', 'ShootlistController@update')->name('Shootlist.update');
// Route::get('/Shootlist', 'ShootlistController@index');


// Route::get('/Contact', 'ContactController@index');
// Route::post('/AddtoContact', 'ContactController@AddtoContact');
// Route::delete('/delete/{id}', 'ContactController@destroy');
// Route::post('/edit/{id}', 'ContactController@edit');
// Route::patch('contact/update/{id}', 'ContactController@update')->name('Contact.update');
// Route::get('/contact', 'ContactController@index');

// Route::get('/Interview', 'InterviewController@index');
// Route::post('/AddInterview', 'InterviewController@AddInterview');
// Route::delete('/delete/{id}', 'InterviewController@destroy');
// Route::post('/edit/{id}', 'InterviewController@edit');
// Route::patch('Interview/update/{id}', 'InterviewController@update')->name('Interview.update');
// Route::get('/Interview', 'InterviewController@index');



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
