@extends('layouts.menu')
@section('content')
<form method="post" action=" {{route('story.update', $story->id)}}" enctype="multipart/form-data">
    @csrf
    @method('PATCH')
    <div class="form-row col-sm-12">
        <div class="form-group col-sm-12">
            <label for="Description">Description</label>
            <input type="text" name="Description" value=" {{$story->Description}} " class="form-control">
        </div>
    </div>
    <div class="form-row col-sm-12">
        <div class="form-group col-sm-12">
            <label for="GFX">GFX</label>
            <input type="text" name="GFX" value=" {{$story->GFX}} " class="form-control">
        </div>
    </div>
    <div class="form-row col-sm-12">
        <div class="form-group col-sm-12">
            <label for="file">file</label>
            <input type="file" name="file" class="form-control">
        </div>
    </div>
    <div class="form-row col-sm-12">
        <div class="form-group col-sm-12">
            <label for="notes">Notes</label>
            <input type="text" name="notes" value=" {{$story->notes}} "class="form-control">
        </div>
    </div>
    <button class="btn btn-dark" type="submit">
        update
    </button>
</form>
    <span>No object received</span>

@endsection