
@extends('layouts.menu')
@section('content')

   
<div class="container col-sm-12">
	
<form method="post" action=" {{url('Addtostore')}} " enctype="multipart/form-data">
	@csrf
	
    <div class="form-row col-sm-12">
        <div class="form-group col-sm-12">
			<label for="Description">Description</label>
			<textarea type="text" name="Description" class="form-control" ></textarea> 
		</div>
        </div>

 <div class="row">
  <div class="col">
  <label class="col-sm-12 control-label" for="Types">Types</label>
  <div class="col-sm-12">
    <select id="Types" name="Types" class="form-control">
      <option value="1">Interview</option>
      <option value="2">Voce-Over</option>
      <option value="3">Talk to Camera</option>
    </select>
  </div>
 </div>


	<div class="col">
			<label for="GFX">GFX</label>
			<input type="text" name="GFX" class="form-control">
		</div>
	</div>

	<div class="form-row col-sm-12">
		<div class="form-group col-sm-12">
			<label for="file">file</label>
			<input type="file" name="file" class="form-control">
		</div>
	</div>
	<div class="form-row col-sm-12">
		<div class="form-group col-sm-12">
			<label for="notes">Notes</label>
			<input type="text" name="notes" class="form-control">
		</div>
	</div>
    <div class="text-sm-center">
	<button class="btn btn-dark" type="submit">
		Add
	</button>
    </div>
</form>
<table class="col-sm-12 text-sm-center" >
        <tr>
            <th>Description</th>
            <th>Types</th>
            <th>GFX</th>
            <th>file</th>
            <th>Notes</th>
            <th>Function</th>
        </tr>
        @if(isset($scenes))
        @forelse ($scenes as $row)
        <tr class="mx-auto">
            <td> {{ $row->Description }} </td>
            <td>
            <?php 

                if($row->Types == 1) {
                     $type ='Interview';
                 } else if ($row->Types==2) { 
                    $type ='Voce-Over';
                } else {
                    $type = 'Talk to Camera';
                } 
             ?> 
             {{$type}}
             </td>
            <td> {{ $row->GFX }} </td>
            <td> 
                <img src=" {{URL::to('/')}}/stories/{{$row->file}} " width="100" height="100" class="img-thumbnail">
            </td>
            <td>  {{ $row->notes }}</td>
        	<td class="d-flex flex-row justify-content-around align-itesm-center p-1 border border-info">
        		<form method="post" action=" delete/{{$row->id}}" class="border border-danger">
        			@csrf
        			@method('DELETE')
        			<button type="submit" class="btn btn-danger">Delete</button>
        		</form> 
        		<form method="post" action="edit/{{$row->id}}" class="border border-danger">
        			@csrf
        			<button  type="submit" class="btn btn-info">Edit</button>
        		</form>
        	</td>
        </tr>
        @empty
            <span>No data has been created yet!</span>
    </table>
        @endforelse
        @endif
      <hr>
</div>
</div>


@endsection