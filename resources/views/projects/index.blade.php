@extends('layouts.app')
@section('content')
    <h1 class="display-3 m-3">Existing Projects</h1>

    @if(Session::has('success'))
        <div class="alert alert-success">{{Session::get('success')}}</div>
    @endif
    
    <table class="table mt-5">
        <div class="text-right mt-3">
            <a class="btn btn-success" href="{{route('projects.create')}}"> Create new project </a>
        </div>
        <thead >
            <tr class="text-center">
               <th scope="col">ID</th>
                <th scope="col">Title</th>
                <th scope="col"> Description</th>
                <th scope="col"> Duration</th>
                <th scope="col"> Started at</th>
                <th scope="col"> Completed At</th>
                <th scope="col" colspan="3">Action</th>
            </tr>
        </thead>

        @foreach ($projects as $project)
            <tbody>
                <tr>
                    <td>{{$project->id}}</td> 
                    <td> <a href="{{url('InformationGathering')}}"> {{$project->title}} </a> </td>
                    <td>{{$project->description}}</td> 
                    <td>{{$project->duration}}</td>               
                    <td>{{$project->start_at}}</td>
                    <td>{{$project->completed_at}}</td> 
                    <td> 
                        <a href="{{route('projects.edit', $project->id)}}" class="btn btn-primary">Edit </a></td>

                    <td> <a href="{{route('projects.show', $project->id)}}" class="btn btn-info">Show</a></td>

                    <td>
                        <form  action="{{route('projects.delete', $project->id)}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger" type="submit">Delete</button>
                        </form>
                    </td> 
                </tr>
            </tbody>
        @endforeach

        {{$projects->links()}}
    
    </table>
@endsection