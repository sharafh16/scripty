@extends('layouts.app')
@section('content')
    <h3 class="display-3 m-3">Project Details</h3>
    
    <div class="p-3">        
        <div class="float-right ">
            <a href="{{route('stories.create',$project->id )}}" class="btn btn-success" >
                Add Story
            </a>
        </div>
        <div class="float-left ">
            <a href="{{ route('projects.index') }}" class="btn btn-info">Back</a>
        </div>
    </div>

    <table class="table my-5">
        <thead >
            <tr class="text-center">
                <th scope="col">ID</th>
                <th scope="col">Title</th>
                <th scope="col"> Description</th>
                <th scope="col"> Duration</th>
                <th scope="col"> Started at</th>
                <th scope="col"> Completed At</th>
                
                
            </tr>
        </thead>

        <tbody>
                <tr>     
                    <td>{{$project->id}}</td> 
                    <td><a href="{{url('InformationGathering')}}">{{$project->title}} </a></td>
                    <td>{{$project->description}}</td>                
                    <td>{{$project->duration}}</td>
                    <td>{{$project->start_at}}</td>
                    <td>{{$project->completed_at}}</td> 
                </tr>
        </tbody>
    </table>
@endsection