
@extends('layouts.menu')
@section('content')

   
<div class="container col-sm-12">
  
<form method="post" action=" {{url('AddShootlist')}} " enctype="multipart/form-data">
  @csrf
<div class="row">
  <div class="col">
      <label for="SceneNumber">Scene No:</label>
      <input type="number" name="SceneNumber" class="form-control" >
    </div>
  
  <div class="col">
      <label for="Date">Date:</label>
      <input type="Date" name="Date" class="form-control">
    </div>
  
  <div class="col">
      <label for="Time">Time:</label>
      <input type="Time" name="Time" class="form-control">
    </div>
  </div>

  <div class="form-row col-sm-12">
    <div class="form-group col-sm-12">
      <label for="Location">Location:</label>
      <input type="text" name="Location" class="form-control">
    </div>
  </div>

  <div class="form-row col-sm-12">
		<div class="form-group col-sm-12">
			<label for="Permissions">Permissions</label>
			<input type="file" name="Permissions" class="form-control">
		</div>
	</div>

    <div class="text-sm-center">
  <button class="btn btn-dark" type="submit">
    Add
  </button>
    </div>
</form>
<table class="col-sm-12 text-sm-center" >
        <tr>
            <th>Scene No</th>
            <th>Date</th>
            <th>Time</th>
            <th>Location</th>
            <th>Permissions</th>
            <th>Function</th>
        </tr>
        @if(isset($Shootlist))
        @forelse ($Shootlist as $row)
        <tr class="mx-auto">
            <td> {{ $row->SceneNumber }} </td>
            <td> {{ $row->Date }} </td>
            <td> {{ $row->Time }} </td>
            <td> {{ $row->Location }} </td>
            <td> 
                <img src=" {{URL::to('/')}}/Shootlist/{{$row->Permissions}} " width="100" height="100" class="img-thumbnail">
            </td>
            
          <td class="d-flex flex-row justify-content-around align-itesm-center p-1 border border-info">
            <form method="post" action=" delete/{{$row->id}}" class="border border-danger">
              @csrf
              @method('DELETE')
              <button type="submit" class="btn btn-danger">Delete</button>
            </form> 
            <form method="post" action="edit/{{$row->id}}" class="border border-danger">
              @csrf
              <button  type="submit" class="btn btn-info">Edit</button>
            </form>
          </td>
        </tr>
        @empty
            <span>No data has been created yet!</span>
    </table>
        @endforelse
        @endif
      <hr>
</div>
</div>


@endsection