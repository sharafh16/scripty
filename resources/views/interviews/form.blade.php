  
    
    @csrf
    {{-- //           title           / --}}
    <div class="form-group">
        <label for="title">Title</label>
        <input type="text" class="form-control @error('title') is-invalid @enderror" id="title" name="title" value="@if(isset($project)) {{$project->title}}  @endif {{old('title')}}">
        @error ('title')
            <p class="text-danger"> {{ $message}} </p>
        @enderror
    </div>

    {{-- //           body_script           / --}}
    <div class="form-group">
        <label for="description">description</label>
        <textarea name="description" id="description" class="form-control @error('description') is-invalid @enderror" cols="30" rows="10" placeholder="Write description here ..." value="@if(isset($project)) {{$project->description}}  @endif  {{old('description')}}"></textarea>  
        @error('description')
            <p class="text-danger"> {{$message}} </p>
        @enderror
    </div>

    <div class="row">
        {{-- //           duration           / --}}
        <div class="col">
            <label for="duration">Duration</label>
            <input type="text" class="form-control @error('duration') is-invalid @enderror" name="duration" id="duration" value="@if(isset($project)) {{$project->duration}}  @endif {{old('duration')}} ">
            @error('duration')
                <p class="text-danger"> {{ $message }} </p>
            @enderror
        </div>
        <div class="col">
            <label for="stert_at">Start at</label>
            <input type="date" class="form-control @error ('start_at')  is-invalid @enderror" name="start_at" id="start_at" value="@if(isset($project)) {{$project->start_at}}  @endif  {{old('sart_at')}}">
            @error('start_at')
                <p class="text-danger"> {{$message }}</p>
            @enderror
        </div>
        
        {{-- //           completed at           / --}}
        <div class="col">
            <label for="completed_at">Completed at</label>
            <input type="date" class="form-control @error ('completed_at')  is-invalid @enderror" name="completed_at" id="completed_at" value="@if(isset($project)) {{$project->completed_at}}  @endif  {{old('completed_at')}}">
            @error('completed_at')
                <p class="text-danger"> {{$message }}</p>
            @enderror
        </div>
    </div>
    <div class="text-sm-center">
    <div class="form-group mt-3">
        <input type="submit" value="Create" class="button" class="btn btn-dark" style="background:none">
    </div>
  </div>