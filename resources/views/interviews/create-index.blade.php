
@extends('layouts.menu')
@section('content')

   
<div class="container col-sm-12">
  
<form method="post" action=" {{url('AddInterview')}} " enctype="multipart/form-data">
  @csrf
  <div class="form-row col-sm-12">
    <div class="form-group col-sm-12">
      <label for="Name">Name:</label>
      <input type="text" name="Name" class="form-control" >
    </div>
  </div>
  <div class="form-row col-sm-12">
    <div class="form-group col-sm-12">
      <label for="Questions">Questions:</label>
      <input type="text" name="Questions" class="form-control">
    </div>
  </div>
  
  <div class="form-row col-sm-12">
    <div class="form-group col-sm-12">
      <label for="ExpectedAnswer">Expected Answer:</label>
      <input type="text" name="ExpectedAnswer" class="form-control">
    </div>
  </div>
    <div class="text-sm-center">
  <button class="btn btn-dark" type="submit">
    Add
  </button>
    </div>
</form>
<table class="col-sm-12 text-sm-center" >
        <tr>
            <th>Name</th>
            <th>Questions</th>
            <th>Expected Answer</th>
            <th>Function</th>
        </tr>
        @if(isset($Interview))
        @forelse ($Interview as $row)
        <tr class="mx-auto">
            <td> {{ $row->Name }} </td>
            <td> {{ $row->Questions }} </td>
           
            <td>  {{ $row->ExpectedAnswer }}</td>
          <td class="d-flex flex-row justify-content-around align-itesm-center p-1 border border-info">
            <form method="post" action=" delete/{{$row->id}}" class="border border-danger">
              @csrf
              @method('DELETE')
              <button type="submit" class="btn btn-danger">Delete</button>
            </form> 
            <form method="post" action="edit/{{$row->id}}" class="border border-danger">
              @csrf
              <button  type="submit" class="btn btn-info">Edit</button>
            </form>
          </td>
        </tr>
        @empty
            <span>No data has been created yet!</span>
    </table>
        @endforelse
        @endif
      <hr>
</div>
</div>


@endsection