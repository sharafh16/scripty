@extends('layouts.menu')
@section('content')
<form method="post" action=" {{route('InfoGathering.update', $InfoGathering->id)}}" enctype="multipart/form-data">
    @csrf
    @method('PATCH')
    <div class="form-row col-sm-12">
        <div class="form-group col-sm-12">
            <label for="Citation">Citation</label>
            <input type="text" name="Citation" value=" {{$InfoGathering->Citation}} " class="form-control">
        </div>
    </div>
    <div class="form-row col-sm-12">
        <div class="form-group col-sm-12">
            <label for="Implied_Information">Implied_Information</label>
            <input type="text" name="Implied_Information" value=" {{$InfoGathering->GFX}} " class="form-control">
        </div>
    </div>
    <div class="form-row col-sm-12">
        <div class="form-group col-sm-12">
            <label for="References">References</label>
            <input type="text" name="References" value=" {{$InfoGathering->References}} "class="form-control">
        </div>
    </div>
    <button class="btn btn-dark" type="submit">
        update
    </button>
</form>
    <span>No object received</span>

@endsection