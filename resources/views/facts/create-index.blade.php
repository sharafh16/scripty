
@extends('layouts.menu')
@section('content')

   
<div class="container col-sm-12">
  
<form method="post" action=" {{url('AddInfo')}} " enctype="multipart/form-data">
  @csrf
  <div class="form-row col-sm-12">
    <div class="form-group col-sm-12">
      <label for="Citation">Citation:</label>
      <input type="text" name="Citation"  class="form-control" cols="30" rows="10"  >
    </div>
  </div>
  <div class="form-row col-sm-12">
    <div class="form-group col-sm-12">
      <label for="Implied Information">Implied Information:</label>
      <input type="text" name="Implied Information" class="form-control">
    </div>
  </div>
  
  <div class="form-row col-sm-12">
    <div class="form-group col-sm-12">
      <label for="References">References:</label>
      <input type="text" name="References" class="form-control">
    </div>
  </div>
    <div class="text-sm-center">
  <button class="btn btn-dark" type="submit">
    Add
  </button>
    </div>
</form>
<table class="col-sm-12 text-sm-center" >
        <tr>
            <th>Citation</th>
            <th>Implied_Information</th>
            <th>References</th>
            <th>Function</th>
        </tr>
        @if(isset($InfoGathering))
        @forelse ($InfoGathering as $row)
        <tr class="mx-auto">
            <td> {{ $row->Citation }} </td>
            <td> {{ $row->Implied_Information }} </td>
           
            <td>  {{ $row->References }}</td>
          <td class="d-flex flex-row justify-content-around align-itesm-center p-1 border border-info">
            <form method="post" action=" deInfo/{{$row->id}}" class="border border-danger">
              @csrf
              @method('DELETE')
              <button type="submit" class="btn btn-danger">Delete</button>
            </form> 
            <form method="post" action="editInfo/{{$row->id}}" class="border border-danger">
              @csrf
              <button  type="submit" class="btn btn-info">Edit</button>
            </form>
          </td>
        </tr>
        @empty
            <span>No data has been created yet!</span>
    </table>
        @endforelse
        @endif
      <hr>
</div>
</div>


@endsection