
@extends('layouts.menu')
@section('content')

   
<div class="container col-sm-12">
  
<form method="post" action=" {{url('AddtoContact')}} " enctype="multipart/form-data">
  @csrf
  <div class="row">
  <div class="col">
      <label for="Name">Name:</label>
      <input type="text" name="Name" class="form-control" >
    </div>
 
   <div class="col">
      <label for="Title">Title: </label>
      <input type="text" name="Title" class="form-control">
    </div>
  </div>
  
  <div class="row">
  <div class="col">
      <label for="Email">Email:</label>
      <input type="text" name="Email" class="form-control">
    </div>
  

  <div class="col">
      <label for="Phone">Phone No:</label>
      <input type="number" name="Phone" class="form-control">
    </div>
  </div>

  

  <div class="row">
  <div class="col">
  <label class="col-sm-12 control-label" for="Status">Status</label>
  <div class="col-sm-12">
    <select id="Status" name="Status" class="form-control">
      <option value="1">Approved</option>
      <option value="2">Declined</option>
      <option value="3">Pending</option>
    </select>
  </div>
</div>

 

<div class="col">
      <label for="Date">Date:</label>
      <input type="date" class="form-control " name="date" id="date" value="  ">
  </div>

  <div class="col">
      <label for="Time">Time:</label>
      <input type="time" class="form-control " name="time" id="time">
  </div>
 </div>
  </div>

 
<br>

  <div class="text-sm-center">
  <button class="btn btn-dark" type="submit">
    Add
  </button>
    </div>
</form>
<table class="col-sm-12 text-sm-center" >
        <tr>
            <th>Name</th>
            <th>Title</th>
            <th>Email</th>
            <th>Phone No</th>
            <th>Status</th>
            <th>Date</th>
            <th>Time</th>
            <th>Function</th>
        </tr>
        @if(isset($Contact))
        @forelse ($Contact as $row)
        <tr class="mx-auto">
            <td> {{ $row->Name }} </td>
            <td> {{ $row->Title }} </td>
            <td> {{ $row->Email }} </td>
            <td> {{ $row->Phone }} </td>
            <td> {{ $row->Status }} </td>
            <td> {{ $row->Date }} </td>
            <td> {{ $row->Time }} </td>
           
           
          <td class="d-flex flex-row justify-content-around align-itesm-center p-1 border border-info">
            <form method="post" action=" delete/{{$row->id}}" class="border border-danger">
              @csrf
              @method('DELETE')
              <button type="submit" class="btn btn-danger">Delete</button>
            </form> 
            <form method="post" action="edit/{{$row->id}}" class="border border-danger">
              @csrf
              <button  type="submit" class="btn btn-info">Edit</button>
            </form>
          </td>
        </tr>
        @empty
            <span>No data has been created yet!</span>
    </table>
        @endforelse
        @endif
      <hr>
</div>
</div>


@endsection