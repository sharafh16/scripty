@extends('layouts.app')
@section('content')
    <h1 class="display-3 m-3">Existing Projects</h1>
    @if(Session::has('message'))
        <div class="alert alert-success">{{Session::get('message')}}</div>
    @endif
    
    <table class="table mt-5">
        <div class="text-right mt-3">
            <a class="btn btn-success" href="{{route('projects.create')}}"> Create new project </a>
        </div>
        <thead >
            <tr class="text-center">
               <th scope="col">ID</th>
                <th scope="col">Title</th>
                <th scope="col"> Description</th>
                <th scope="col"> Duration</th>
                <th scope="col"> Started at</th>
                <th scope="col"> Completed At</th>
                
                <th scope="col" colspan="3">Action</th>
            </tr>
        </thead>
        @foreach ($projects as $project)
            <tbody>

                
                    <tr>
                
                       
                             <td>{{$project->id}}</td> 
                        <td>
                                {{$project->title}} 
                        </td>
                        
                        <td>{{$project->description}}</td> 
                        <td>{{$project->duration}}</td>               
                        <td>{{$project->start_at}}</td>
                        <td>{{$project->completed_at}}</td> 
                        <td> 
                            <form  action="{{route('projects.edit')}}" method="POST">
                                @csrf
                                <input type="hidden" name="project_id" value="{{$project->id}}">
                                <button class="btn btn-primary" type="submit">edit</button>
                            </form>
                        </td>
                        <td>
                            <form  action="{{route('projects.destroy')}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="hidden" name="project_id" value="{{$project->id}}">
                                <button class="btn btn-danger" type="submit">delete</button>
                            </form>
                        </td> 
                        <td>
                            <form  action="{{url('bodyScript')}}" method="POST">
                                @csrf
                                <input type="hidden" name="project_id" value="{{$project->id}}">
                                <button class="btn btn-info" type="submit">Open</button>
                            </form>
                        </td> 
                    </tr>
            </tbody>
        @endforeach
    </table>
@endsection