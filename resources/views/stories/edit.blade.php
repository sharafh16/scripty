@extends('layouts.app')
@section('content')

	<div class="container m-5">
	    
		<form method="POST" action=" {{ route('stories.update') }} ">
		    @csrf
		    @method('PATCH')
		    
		    	{{-- Form inputs --}}
		     <input type="hidden" name="story_id" value="{{$story->id}}">
			 <div class="form-row col-sm-12">
			      <div class="form-group col-sm-12">
			        <label for="Description">Description</label>
			        <textarea type="text" name="description" class="form-control" cols="30" rows="10" value="{{$story->description}}"  ></textarea> 
			      </div>
			  </div>
		 	
		 	{{-- Buttons --}}
		     <div class="text-sm-center">
		        <input type="submit" value="Update"  class="btn btn-success">

		        <a href="{{route('stories.index')}}"  class="btn btn-danger"> Cancel </a>
		     </div>

		</form>

	</div>

@endsection