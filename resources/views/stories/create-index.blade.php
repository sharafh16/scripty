
@extends('layouts.menu')
@section('content')
  
  <div class="container col-sm-12">
        @if(Session::has('success'))
            <div class="alert alert-success">{{ Session::get('success') }}</div>
        @endif    
                      {{-- Create Story --}}
        <form method="POST" action=" {{ route('stories.store') }} ">
            @csrf
             <div class="form-row col-sm-12">
              <div class="form-group col-sm-12">
                <label for="Description">Description</label>
                <textarea type="text" name="description" class="form-control" cols="30" rows="10" placeholder="Write Story here ..."  ></textarea> 
              </div>
              </div>
             <div class="text-sm-center">
                <input type="submit" value="Add" class="btn btn-success">
             </div>
        </form>

                        {{-- Index Story --}}
        
        <table class="col-sm-12 text-sm-center mt-5 my-5">

            <thead>
              <tr>
                  <th>Story ID</th>
                  <th>Description</th>
                  <th>Project ID</th>
                  <th>Project Title</th>
                  <th>Actions</th>
              </tr>
            </thead>
           
            <tbody>

              @if(isset($stories))
                  
                  @foreach ($stories as $story)
                  <tr class="mx-auto">
                      <td> {{ $story->id }} </td>
                      <td> {{ $story->description }} </td>
                      <td> {{ $story->project->id }} </td>
                      <td> {{ $story->project->title }} </td>
                      <td> 
                          <form method="post" action="{{route('stories.delete',$story->id )}}" class=" d-inline">
                              @csrf
                              @method('DELETE')
                              <input type="hidden" name="project_id" value="{{ $story->project->id }}">
                              <button type="submit" class="btn btn-danger">Delete</button>
                          </form> 
                              <a  class="border border-dark btn btn-info" href="{{route('stories.edit', $story->id)}}">Edit</a>

                      </td>
                  </tr>
                  
                  @endforeach
              
              @else

                <span>No data has been created yet!</span>

              @endif
            </tbody>
        </table>   
  </div>

@endsection